<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h3>Ejercicio 1</h3>
    <?php
    echo "Crear el array";
    $pruebaArray = array("rogger","fabian","jesus","santi","raul");
    ?>
    <br>
    <h3>Ejercicio 2</h3>
    <?php
    echo count($pruebaArray);
    echo "<br>";
    ?>
    <br>
    <h3>Ejercicio 3</h3>
    <?php 
    echo implode(" ",$pruebaArray)

    ?>
    <h3>Ejercicio 4</h3>
    <?php 
    $nombreOrdenado = $pruebaArray;
     asort($nombreOrdenado);
     print_r($nombreOrdenado);

    ?>
    <h3>Ejercicio 5</h3>
    <?php 
    $testeoS = array();
    $testeoS["nombre"] = "santi";
    $testeoS["clase"] = "daw";
    $testeoS["asignatura"] = "dsw";
      sort($testeoS);
      print_r($testeoS);
     echo "<br>";
     $testeoK = array();
    $testeoK["nombre"] = "santi";
    $testeoK["clase"] = "daw";
    $testeoK["asignatura"] = "dsw";
     ksort($testeoK);
     print_r($testeoK);
    echo "<br>";

    echo "El primer ejemplo esta hecho con la funcion sort() la cual ordeno el array por sus valores, en cambio ksort ordeno el array segun el valor de sus claves";

    ?>
    <h3>Ejercicio 6</h3>
    <?php 
    $reves = $pruebaArray;
     $reves = array_reverse($reves);
     print_r($reves);

    ?>

<h3>Ejercicio 7</h3>
    <?php 
    echo array_search("santi",$pruebaArray);

    ?>

<h3>Ejercicio 8</h3>
    <?php 
    $datosAlumnos = array(
        'santi' => array(
        'id' => "1",
        'nombre' => "santi",
        'edad' => "20"),
        'fabian' => array(
            'id' => "2",
            'nombre' => "Fabian",
            'edad' => "19"),
            'ismael' => array(
                'id' => "3",
                'nombre' => "Ismael",
                'edad' => "21"),
        
    );
    
    echo "Crear un array de arrays";

    ?>

<h3>Ejercicio 9</h3>
    <?php 
   

    

echo "<table>";
echo "<tr>";
echo "  <th> id  </th>";
echo "  <th> Nombre </th>";
echo "  <th> Edad </th>";
echo "</tr>";
echo "<tr>";
echo "<td> " . $datosAlumnos["santi"]["id"] . "</td>";
echo "<td> " . $datosAlumnos["santi"]["nombre"] . "</td>";
echo "<td> " . $datosAlumnos["santi"]["edad"] . "</td>";
echo "</tr>";
echo "<tr>";
echo "<td> " . $datosAlumnos["fabian"]["id"] . "</td>";
echo "<td> " . $datosAlumnos["fabian"]["nombre"] . "</td>";
echo "<td> " . $datosAlumnos["fabian"]["edad"] . "</td>";
echo "</tr>";
echo "<tr>";
echo "<td> " . $datosAlumnos["ismael"]["id"] . "</td>";
echo "<td> " . $datosAlumnos["ismael"]["nombre"] . "</td>";
echo "<td> " . $datosAlumnos["ismael"]["edad"] . "</td>";
echo "</tr>";
echo "</table>";
?>

<h3>Ejercicio 11</h3>
    <?php 
    
    $arraySuma = array(1,5,7,2);
    echo array_sum($arraySuma);
    ?>

</body>
</html>